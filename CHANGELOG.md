## 0.4 / 2019-04-15

### Added

* Downloadable artifacts (eval/ohmyclj!3)
* Show usage via help

### Fixed

* Failure starting repl when no test-ns (eval/ohmyclj#3)

## 0.3 / 2019-03-21

### Fixed

* Syntax error bash-3.2 when running test (eval/ohmyclj#1)

## 0.2 / 2019-03-19

* initial release

## version / YYYY-MM-DD

### Added/Fixed/Deprecated/Removed

  * some line

    some sample
