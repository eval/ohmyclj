# ohmyclj

Dev/test/run Clojure scripts.


[![pipeline status](https://gitlab.com/eval/ohmyclj/badges/master/pipeline.svg)](https://gitlab.com/eval/ohmyclj/commits/master)
[![discuss at Clojurians-Zulip](https://img.shields.io/badge/Clojurians%20Zulip-ohmyclj-brightgreen.svg)](https://clojurians.zulipchat.com/#narrow/stream/192170-ohmyclj)

## Quick Intro

Given the executable script `hello_world.clj`:
```clojure
#!/usr/bin/env ohmyclj
"DEPS='clj-time=0.15.1'"

(ns user
  (:require [clj-time.core :as t]))

(defn -main [& args]
  (println (str "Hello, " (nth args 0 "World") "! Time is now " (t/now))))

(ns test
  (:require [clojure.test :refer [deftest is testing are]]
            [user]))

(deftest main-test
  (is (some? (user/-main "Foo")) "FIXME"))
```

ohmyclj allows you to:
- run it: `./hello_world.clj Clojurians` (e.g. `Hello, Clojurians! Time is now 2019-04-15T14:47:27.235Z`)
- try it: `ohmyclj repl hello_world.clj` (iterate quickly with helper-functions like `(reload)`)
- test it: `ohmyclj test hello_world.clj`

Try it yourself: `ohmyclj new > hello_word.clj`

## Install

Requirements:
- [Clojure](https://www.clojure.org/guides/getting_started#_clojure_installer_and_cli_tools)

Put `ohmyclj` on your path:
``` shell
# download
# ...latest stable release
$ curl -sL https://gitlab.com/eval/ohmyclj/-/jobs/artifacts/latest/raw/bin/ohmyclj?job=release > ~/bin/ohmyclj
# ...or master (there will be dragons...)
$ curl -sL https://gitlab.com/eval/ohmyclj/-/jobs/artifacts/master/raw/bin/ohmyclj?job=release > ~/bin/ohmyclj-edge

$ chmod +x ~/bin/ohmyclj*
```

**NOTE**: ohmyclj is still pretty young so breaking changes _may_ happen. Keep an eye on the [CHANGELOG](CHANGELOG.md) when doing upgrades.  

### Install in a project

Adding the ohmyclj-script to a project has the benefit that:
- team-members don't need to install it
- all scripts keep working even if breaking changes or bugs are introduced upstream


Here's how to do it:
```shell
# install in scripts-folder
$ mkdir -p $PROJECT/scripts
$ curl -sL https://gitlab.com/eval/ohmyclj/-/jobs/artifacts/latest/raw/bin/ohmyclj?job=release > scripts/ohmyclj
$ chmod +x scripts/ohmyclj
```

Now you can invoke other scripts from the project-root via `./scripts/ohmyclj run path/to/other/script.clj`.  
In order to prevent invocation via `./path/to/other/script.clj` (which will not work if ohmyclj is not globally installed, or use a different version when it is),
make the script not executable, or remove the hash-bang (ie first line) of the generated script.

If the whole team is using a tool like [direnv](https://direnv.net/) that loads environment variables depending on the current directory (highly recommended!), not much needs changing.

Just add the following to `$PROJECT/.envrc`
```bash
# add scripts-folder to $PATH
PATH_add scripts
```
Now when invoking `ohmyclj` from any path 'under' `$PROJECT`, the project-version will be used. Hash-bangs also work as expected.


## Usage

### New

Generate new script:

``` shell
$ ohmyclj new > hello_world.clj
$ chmod +x hello_world.clj
```

Let's take a closer look at `hello_world.clj`:
1. dependencies
    Dependencies for a script are added to "DEPS=...".
    Multiple dependencies are separated by `;`, e.g. `"DEPS='clj-time=0.15.0;some-other=1.2.3'"`

1. user/-main
    This is the starting point of the script. It's invoked with the command line arguments.

### Run

``` shell
$ ./hello_world.clj 1 2 3
:args ("1" "2" "3")
```

### Test

Change the tests so you'll see something like this:

``` shell
$ ohmyclj test hello_world.clj
Testing test

Ran 1 tests containing 1 assertions.
0 failures, 0 errors.
```

### Repl

Fire up a repl with [Rebel readline editor](https://github.com/bhauman/rebel-readline):
```shell
$ ohmyclj repl hello_world.clj
#'user/reload
#'user/run-tests
[Rebel readline] Type :repl/help for online help info
user=>
```

Two helper functions are at your disposal:
- reload - to get the latest changes to the script in the repl
- run-tests - to run the tests


## License

See [LICENSE](LICENSE).
