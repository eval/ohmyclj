ohmyclj_test() {
  script="$1"
  shift
  echo "$BATS_TEST_DIRNAME"
  run "${BATS_TEST_DIRNAME}/../../libexec/ohmyclj" test "${BATS_TEST_DIRNAME}/${script}"
}


@test "passing_tests.clj: succeeds and reports failures" {
  ohmyclj_test 'passing_tests.clj'

  [ $status -eq 0 ]

  [[ "$output" =~ "0 failures, 0 errors" ]]
}

@test "failing_tests.clj: fails and reports failures" {
  ohmyclj_test 'failing_tests.clj'

  [ $status -eq 1 ]

  [[ "$output" =~ "1 failures," ]]
}


@test "no_tests.clj: fails and reports test-ns not found" {
  ohmyclj_test 'no_tests.clj'

  [ $status -eq 1 ]

  [[ "$output" =~ "No namespace: test found" ]]
}
