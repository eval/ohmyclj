ohmyclj_run() {
  script="$1"
  shift
  run "${BATS_TEST_DIRNAME}/../../libexec/ohmyclj" run "${BATS_TEST_DIRNAME}/${script}" "$@"
}

@test "e2e tools-deps version" {
  echo "# tools-deps-version $(grep Version `which clojure`)" >&3
}

@test "sample.clj" {
  ohmyclj_run "sample.clj"

  [ $status -eq 0 ]
  [ "$output" == "Hello World" ]
}

@test "with-main.clj: no args" {
  ohmyclj_run 'with-main.clj'

  [ $status -eq 0 ]
  [ "$output" == ":args nil" ]
}

@test "with-main.clj: with args" {
  ohmyclj_run 'with-main.clj' 1 2 'and 3'
  #echo $output
  [ $status -eq 0 ]
  [ "$output" == ":args (\"1\" \"2\" \"and 3\")" ]
}

@test "with-deps.clj" {
  ohmyclj_run 'with-deps.clj'
  

  [ $status -eq 0 ]

  #echo $output
  [[ "$output" =~ "1986-10-14T00:00:00.000Z" ]]
}
